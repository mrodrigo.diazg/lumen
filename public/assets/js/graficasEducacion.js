var avanceEdu1 = <?php echo $avanceEdu1; ?>;
var totalBenEdu1 = <?php  echo $totalBenEdu1; ?>;
var faltanteEdu1 = totalBenEdu1 - avanceEdu1;

var avanceSalud1 = <?php echo $avanceSalud1; ?>;
var totalBenSalud1 = <?php  echo $totalBenSalud1; ?>;
var faltanteSalud1 = totalBenSalud1 - avanceSalud1;

var avanceCE1 = <?php echo $avanceCE1; ?>;
var totalBenCE1 = <?php  echo $totalBenCE1; ?>;
var faltanteCE1 = totalBenCE1 - avanceCE1;

var avanceSBV1 = <?php echo $avanceSBV1; ?>;
var totalBenSBV1 = <?php  echo $totalBenSBV1; ?>;
var faltanteSBV1 = totalBenSBV1 - avanceSBV1;

var avanceAA1 = <?php echo $avanceAA1; ?>;
var totalBenAA1 = <?php  echo $totalBenAA1; ?>;
var faltanteAA1 = totalBenAA1 - avanceAA1;

var avanceIP1 = <?php echo $avanceIP1; ?>;
var totalBenIP1 = <?php  echo $totalBenIP1; ?>;
var faltanteIP1 = totalBenIP1 - avanceIP1;

var avanceSS1 = <?php echo $avanceSS1; ?>;
var totalBenSS1 = <?php  echo $totalBenSS1; ?>;
var faltanteSS1 = totalBenSS1 - avanceSS1;

    $(function () {
        $('#educacion').highcharts({
            chart: {
                type: 'column'

            },
            title: {
                text: 'Estadisticas Nacionales'
            },
            xAxis: {
                categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total fruit consumption'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -30,
                verticalAlign: 'top',
                y: 25,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black'
                        }
                    }
                }
            },
            series: [{
                name: 'Restante',
                data: [faltanteEdu1, 3, 4, 7, 2]
            }, {
                name: 'Avance',
                data: [avanceEdu1, 2, 3, 2, 1]
            }]
        });
    });
$('.highcharts-series-0 rect').attr({
            fill: 'yellow',
        })
        .add();