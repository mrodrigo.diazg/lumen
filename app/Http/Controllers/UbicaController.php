<?php

namespace App\Http\Controllers;

#use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UbicaController extends Controller
{

    public function tiendasfonart(){

        return view('user.ubicacion.fonart');
    }
        public function tiendasdiconsa(){

        return view('user.ubicacion.diconsa');
    }
            public function pei(){

        return view('user.ubicacion.pei');
    }
                public function delegaciones(){

        return view('user.ubicacion.delegaciones');
    }

}