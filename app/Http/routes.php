<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app->get('/', 'HomeController@index');


//$app->get('enlace', 'EnlaceQuejaController@create');

/*
|--------------------------------------------------------------------------
| Application Routes
|-------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


/*
|--------------------------------------------------------------------------
| AdminController Routes
|-------------------------------------------------------------------
*/
    $app->get('admin/dashboard', 'AdminController@dashboard');
    $app->get('admin/users', 'AdminController@users');


$app->get('home', 'HomeController@index');
/*
|--------------------------------------------------------------------------
| UserController Routes
|-------------------------------------------------------------------
*/
    $app->get('user/home', 'UserController@index');
    $app->get('Sedesol', 'UserController@sedesol');
    $app->get('Donde-Estamos', 'UserController@dondeEstamos');
    $app->get('Programas-Sociales', 'UserController@programasSociales');
    $app->get('Beneficios', 'UserController@beneficios');
    $app->get('Tramites', 'UserController@tramites');
    $app->get('Quejas-Denuncias', 'UserController@quejasDenuncias');
    $app->get('Noticias', 'UserController@noticias');
    $app->get('Contacto', 'UserController@contacto');
/*
|--------------------------------------------------------------------------
| UserController Routes
|-------------------------------------------------------------------
*/

$app->get('Programas-Sociales/conadis', 'ProgramasController@conadis');
$app->get('Programas-Sociales/imjuve', 'ProgramasController@imjuve');
$app->get('Programas-Sociales/inaes', 'ProgramasController@inaes');
$app->get('Programas-Sociales/fonart', 'ProgramasController@fonart');
$app->get('Programas-Sociales/diconsa', 'ProgramasController@diconsa');
$app->get('Programas-Sociales/liconsa', 'ProgramasController@liconsa');
$app->get('Programas-Sociales/prospera', 'ProgramasController@prospera');
$app->get('Programas-Sociales/indesol', 'ProgramasController@indesol');
$app->get('Programas-Sociales/inapam', 'ProgramasController@inapam');
$app->get('Programas-Sociales/pei', 'ProgramasController@pei');
$app->get('Programas-Sociales/dgpop', 'ProgramasController@dgpop');
$app->get('Programas-Sociales/svjf', 'ProgramasController@svjf');
$app->get('Programas-Sociales/ppam', 'ProgramasController@ppam');
$app->get('Programas-Sociales/paja', 'ProgramasController@paja');
$app->get('Programas-Sociales/pet', 'ProgramasController@pet');
$app->get('Programas-Sociales/tres', 'ProgramasController@tres');
$app->get('Programas-Sociales/comedores', 'ProgramasController@comedores');

$app->get('Donde-Estamos/fonart', 'UbicaController@tiendasfonart');
$app->get('Donde-Estamos/diconsa', 'UbicaController@tiendasdiconsa');
$app->get('Donde-Estamos/pei', 'UbicaController@pei');
$app->get('Donde-Estamos/delegaciones', 'UbicaController@delegaciones');