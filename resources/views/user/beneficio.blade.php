@extends('layouts.app')
<link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="/assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="/assets/css/normalize.css">
<link rel="stylesheet" href="/assets/css/main.css">
<link rel="stylesheet" href="/assets/css/jquery.steps.css">
<style>
	.wizard > .actions {
		top:-150px;
	}
</style>
<style>
body
{
        background: url("/assets/image/ppam.png")  fixed center !important;
}
.tresd
{
-moz-box-shadow: 0 0 5px 5px #AD0056;
-webkit-box-shadow: 0 0 5px 5px #AD0056;
box-shadow: 0 0 5px 5px #AD0056;
        border-radius:50px !important;
}
.back-header
{
  background-color:#AD0056;
}
.black-head
    {
        background: rgba(0,0,0,0.5);
        padding-bottom:20px;
        position:relative;
        top:-55px;
    }
.top10
    {
        margin-top:10px;
    }

</style>


@include('partials/top',array())
@section('content')

    <div class="row black-head" >
        <div class="col-md-12 col-xs-12 title_ins2 text-center">
            <h1>ATENCIÓN AL BENEFICIARIO</h1>
        </div>
    </div>
<div class="row ">
    <input type="hidden" name="genero" id="genero" value="" />
    <input type="hidden" name="edad" id="edad" value="" />
    <input type="hidden" name="civil" id="civil" value="" />
    <input type="hidden" name="laboras" id="laboras" value="" />
    <input type="hidden" name="hijos" value="" />
<div class="col-md-12 col-xs-12 back blank " style="margin-bottom:120px; padding-top:20px;">
	<div class="content">
		<form>
		    <div id="wizard">
		        <h2>SELECCIONA EL GÉNERO AL QUE PERTENECES</h2>
		        <section>
		            <h4>SELECCIONA EL GÉNERO AL QUE PERTENECES</h4>
		            <div class="col-md-12 text-center">
		            	<div class="question1">
		            		<div class="top10 col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
								<h1>HOMBRE</h1>
								<span><input id="req1" type="radio" name="grupo" value="hombre" >
                                    @include('partials.beneficio.pregunta1.hombre',array())
								</span>
			            	</div>
			            	<div class=" top10 col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<h1>MUJER</h1>
								<span>
								<input id="req2" class="req2" type="radio" name="grupo" value="mujer" >
                                    @include('partials.beneficio.pregunta1.mujer',array())								
								</span>
			            	</div>  
                            		            </div>
                        <div class="col-md-12 text-center">
                 <div  class="lactancia">
                        <section>
                            <h5>¿ESTÁS EN PERÍODO DE LACTANCIA O GESTACIÓN?</h5>
                            <div class="img">
                                <div class="col-md-6">
                                    <label><input id="req9" type="radio" name="lactancia" value="hombre" > Sí </label>
                                </div>
                                <div class="col-md-6">
                                    <label><input id="req9" type="radio" name="lactancia" value="hombre" > No </label>
                                </div>
                            </div>
                        </section>
                            </div>
		            	</div>
                    </div>
		        </section>



		        <h2>¿CUÁL ES TU EDAD?</h2>
		        <section>
					<h4>¿CUÁL ES TU EDAD?</h4>
                    <div class="row">
                        <div class="col-md-12">
                             <select id="tuedad" name="tuedad" class="form-control">
                            <option value="">Selecciona una opción</option>
                        </select>
                        </div>
                    </div>
					<div class="img">
                        @include('partials.beneficio.pregunta2.edad',array())
                            
                       
					</div>
		        </section>

		        <h2>SELECCIONA EL ESTADO CIVIL AL QUE PERTENECES</h2>
		        <section>
		     		<h4>SELECCIONA EL ESTADO CIVIL AL QUE PERTENECES</h4>
		     		<div class="img col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                        @include('partials.beneficio.pregunta3.soltero',array())
						
						<h2 class="text-center">Soltero</h2>

		     		</div>
		     		<div class="img col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                                @include('partials.beneficio.pregunta3.casado',array())
						
						<h2 class="text-center">Casado</h2>
		     		</div>
		     		<div class="img col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                         @include('partials.beneficio.pregunta3.unionlibre',array())
						
		     		</div>
		        </section>

		        <h2>¿ACTUALMENTE LABORAS?</h2>
		        <section>
		  			<h4>¿ACTUALMENTE LABORAS?</h4>
		  			<div class="img col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
						@include('partials.beneficio.pregunta4.si',array())
						<h2 class="text-center">SI</h2>
		  			</div>
		  			<div class="img col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
						@include('partials.beneficio.pregunta4.no',array())
						<h2 class="text-center">NO</h2>
		  			</div>
		        </section>
		        <h2>¿TIENES HIJOS O ERES RESPONSABLE LEGAL DE ALGÚN MENOR DE EDAD?</h2>
		        <section>
		  			<h4>¿TIENES HIJOS O ERES RESPONSABLE LEGAL DE ALGÚN MENOR DE EDAD?</h4>
		        </section>

		        <h2>SELECCIONA EL TIPO DE ZONA DONDE VIVES</h2>
		        <section>
		  			<h4>SELECCIONA EL TIPO DE ZONA DONDE VIVES</h4>
		  			<div class="img col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                            @include('partials.beneficio.pregunta6.rural',array())
						<h2 class="text-center">RURAL</h2>
		  			</div>
		  			<div class="img col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
						 @include('partials.beneficio.pregunta6.urbana',array())
						<h2 class="text-center">URBANA</h2>

		  			</div>
		  			<br><br><br><br>
		        </section>
		        <h2>¿CONSIDERAS QUE VIVEN MÁS DE 15000 HABITANTES EN TU COMUNIDAD?</h2>
		        <section>
		            <h4>¿CONSIDERAS QUE VIVEN MÁS DE 15000 HABITANTES EN TU COMUNIDAD?</h4>
		            <div class="col-md-12 text-center">
		            	<div class="question1">
		            		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">

                                    <label><input id="req10" type="radio" name="habitantes" value="1" > Sí </label>
			            	</div>
			            	<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <label><input id="req10" type="radio" name="habitantes" value="0" > No </label>
			            	</div>
		            	</div>

		            	<hr>
		            </div>
		        </section>
		        <button type="button" class="btn btn-success" onclick="diagnostico()" style="display:none;">Aceptar</button>
		    </div>
	    </form>
	</div>
</div>
</div>
@section('modals')

@endsection

@section('js-extras')
<script src="/assets/js/modernizr-2.6.2.min.js"></script>
<script src="/assets/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="/assets/js/jquery.cookie-1.3.1.js"></script>
<script src="/assets/js/jquery.steps.js"></script>
<script>
/*
$( ".req2" ).on( "click", function() {

  alert('funciona');
  console.log('funciona');
});
*/


$(function (){
    var maxima=115;
    var html="";
    for(var i=0; i <= maxima; i++ )
    {
       html= html + '<option value="'+i+'">'+i+'</option>';  
    }
    $("#tuedad").html(html);
    $(".lactancia").hide();
    $("#wizard").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical"
    });
});
$(document).on("click","#req2",function()
{
   var valor=$('input[name=grupo]:checked').val();
   if(valor=="mujer")
    {
        $(".lactancia").show();
        $("#genero").val("1");
    }
});
$(document).on("click","#req1",function()
{
   var valor=$('input[name=grupo]:checked').val();
   if(valor=="hombre")
    {
        $(".lactancia").hide();
        $("#genero").val("0");
    }
});

</script>
@endsection

@endsection    