@extends('layouts.programas')
<style>
body
{
        background: url("/assets/image/ppam.png")  fixed center !important;
}
.tresd
{
-moz-box-shadow: 0 0 5px 5px #AD0056;
-webkit-box-shadow: 0 0 5px 5px #AD0056;
box-shadow: 0 0 5px 5px #AD0056;
        border-radius:50px !important;
}
.back-header
{
  background-color:#AD0056;
}
.black-head
    {
        background: rgba(0,0,0,0.5);
        padding-bottom:20px;
        position:relative;
        top:-55px;
    }
</style>
@include('partials/top',array())
@section('content')


<div class="row black-head">
    <div class="col-md-12 col-xs-12 title_ins2 text-center">
        <h1 >PROGRAMAS SOCIALES</h1>
    </div>
</div>
<div class=" col-md-12 blank">
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/conadis') }}">
		<img src="/assets/image/programas-sociales/logos_sectorizados_NORMAL-01.png" alt="" class="img-responsive">
	</a>
</div>

<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('home') }}">
		<img src="/assets/image/programas-sociales/logos_sectorizados_NORMAL-09.png" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/inaes') }}">
		<img src="/assets/image/programas-sociales/logos_sectorizados_NORMAL-02.png" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/fonart') }}">
		<img src="/assets/image/programas-sociales/logos_sectorizados_NORMAL-03.png" alt="" class="img-responsive">
	</a>

</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/diconsa') }}">
		<img src="/assets/image/programas-sociales/logos_sectorizados_NORMAL-04.png" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/liconsa') }}">
		<img src="/assets/image/programas-sociales/logos_sectorizados_NORMAL-05.png" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/prospera') }}">
		<img src="/assets/image/programas-sociales/logos_sectorizados_NORMAL-06.png" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('home') }}">
		<img src="/assets/image/programas-sociales/logos_sectorizados_NORMAL-07.png" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/inapam') }}">
		<img src="/assets/image/programas-sociales/logos_sectorizados_NORMAL-08.png" alt="" class="img-responsive">
	</a>
</div>

<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/pei') }}">
		<img src="/assets/image/programas-sociales/logos_centralizados_NORMAL-01.png" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
 	<a href="{{ URL('Programas-Sociales/dgpop') }}">
 		<img src="/assets/image/programas-sociales/logos_centralizados_NORMAL-02.png" alt="" class="img-responsive">
 	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/svjf') }}">
		<img src="/assets/image/programas-sociales/logos_centralizados_NORMAL-03.png" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/ppam') }}">
		<img src="/assets/image/programas-sociales/logos_centralizados_NORMAL-04.png" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
 	<a href="{{ URL('Programas-Sociales/paja') }}">
 		<img src="/assets/image/programas-sociales/logos_centralizados_NORMAL-05.png" alt="" class="img-responsive">
 	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/pet') }}">
		<img src="/assets/image/programas-sociales/logos_centralizados_NORMAL-06.png" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/tres') }}">
		<img src="/assets/image/programas-sociales/logos_centralizados_NORMAL-07.png" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/comedores') }}">
		<img src="/assets/image/programas-sociales/logos_centralizados_NORMAL-08.png" alt="" class="img-responsive">
	</a>
</div>
</div>


@endsection
@section('modals')
@endsection
@section('js-extras')

@endsection