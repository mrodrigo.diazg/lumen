<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Noticias</title>

  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <link rel="shortcut icon" href="favicon.ico">

  <!-- Fonts START -->
  <!--link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css"-->
  <!-- Fonts END -->

  <!-- Global styles START -->
  <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Global styles END -->
    <style>
body
{
    background: url("/assets/image/textura_fondo-01.png")  fixed center !important;
}
.tresd
{
-moz-box-shadow: 0 0 5px 5px #AD0056;
-webkit-box-shadow: 0 0 5px 5px #AD0056;
box-shadow: 0 0 5px 5px #AD0056;
        border-radius:50px !important;
}
.back-header
{
  background-color:#AD0056;
}
.black-head
{
     background: rgba(0,0,0,0.5);
     padding-bottom:20px;
     position:relative;
     top:-55px;
}
</style>

  <!-- Page level plugin styles START -->
  <link href="/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
  <!-- Page level plugin styles END -->

  <!-- Theme styles START -->
  <link href="/assets/global/css/components.css" rel="stylesheet">
  <link href="/assets/frontend/layout/css/style.css" rel="stylesheet">
  <link href="/assets/frontend/layout/css/style-responsive.css" rel="stylesheet">
  <link href="/assets/frontend/layout/css/themes/red.css" rel="stylesheet" id="style-color">
  <link href="/assets/frontend/layout/css/custom.css" rel="stylesheet">
 <style>
 body
  {
      background: url("/assets/image/textura_fondo-01.png")  fixed center !important;
  }

</style>
  <!-- Theme styles END -->
</head>
<!-- Head END -->
@include('partials/top',array())
<!-- Body BEGIN -->

<body class="corporate page-header-fixed">
    <!-- BEGIN HEADER -->

    <!-- Header END -->
<div class="row black-head">
    <div class="col-md-12 col-xs-12 title_ins2">
        <h1 >¿DÓNDE ESTAMOS?</h1>
    </div>
</div>
    <div class="main " >
    <br>
      <div class="container" style=" background:#fff; padding-top:-30px;">
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40" >
          <!-- BEGIN CONTENT -->
          <div class="col-md-12 col-sm-12" >
            <h1 >Actualidad</h1>
            <div class="content-page">
              <div class="row">
                <!-- BEGIN SERVICE BLOCKS -->
                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 ">
                  <div class="row margin-bottom-20">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div class="service-box-v1">
                        <div><img src="/assets/image/noticias/bandera_blanca.jpg" alt="" class="img-responsive"></div>
                        <h2 style="font-size:18px;">Levanta Sedesol “Bandera Blanca” por cobertura total de programas de Adultos Mayores y Estancias Infantiles en Durango</h2>
                        <p></p>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div class="service-box-v1">
                        <div><img src="/assets/image/noticias/jalisco.JPG" alt="" class="img-responsive"></div>
                        <h2 style="font-size:18px;">Sedesol inicia la Jornada Nacional de Alimentación en el estado de Jalisco</h2>
                        <p></p>
                      </div>
                    </div>
                  </div>
                  <div class="row margin-bottom-20">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div class="service-box-v1">
                        <div><img src="/assets/image/noticias/queretaro.jpg" alt="" class="img-responsive"></div>
                        <h2 style="font-size:18px;">Arranca Sedesol Jornada Nacional de Alimentación en Querétaro</h2>
                        <p></p>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div class="service-box-v1">
                        <div><img src="/assets/image/noticias/FND_.jpg" alt="" class="img-responsive"></div>
                        <h2 style="font-size:18px;">Mensaje del srio. de Desarrollo Social, José A. Meade, en la Firma de Convenio entre la Sedesol y la FND</h2>
                        <p></p>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END SERVICE BLOCKS -->

                <!-- BEGIN VIDEO AND TESTIMONIALS -->
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                  <!-- BEGIN VIDEO -->
                  <iframe height="270" allowfullscreen="" style="width:100%; border:0" src="http://player.vimeo.com/video/127559024?portrait=0" class="margin-bottom-10"></iframe>
                  <!-- END VIDEO -->

                <!-- BEGIN TESTIMONIALS -->
                <div class="testimonials-v1 testimonials-v1-another-color">
                  <h2></h2>
                  <div id="myCarousel1" class="carousel slide">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                      <div class="active item">
                        <blockquote><p>Más allá de mediciones, el reto es dotar de alimentación sana a 36 millones de mexicanos: Meade Kuribreña</p></blockquote>
                        <div class="carousel-info">
                          <!--img class="pull-left" src="../../assets/frontend/pages/img/people/img1-small.jpg" alt=""-->
                          <div class="pull-left">
                            <span class="testimonials-name"></span>
                            <span class="testimonials-post"></span>
                          </div>
                        </div>
                      </div>

                    </div>
                    <!-- Carousel nav -->
                    <!--a class="left-btn" href="#myCarousel1" data-slide="prev"></a-->
                    <!--a class="right-btn" href="#myCarousel1" data-slide="next"></a-->
                  </div>
                </div>
                <!-- END TESTIMONIALS -->
                </div>
                <!-- END BEGIN VIDEO AND TESTIMONIALS -->
              </div>



            </div>
          </div>
        </div>
      </div>
    </div>



    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <script src="../../assets/global/plugins/respond.min.js"></script>
    <![endif]-->
    <script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/assets/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->

    <script src="/assets/frontend/layout/scripts/layout.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();
            Layout.initTwitter();
        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>