    @extends('layouts.app')
    @section('style')
     <style>
     #map { min-height: 400px; width:100%; }
     body
      {
          background: url("/assets/image/textura_fondo-01.png")  fixed center !important;
      }
      .back{
        background: #fff;
        padding: 40px;
        padding-bottom: 20px;
      }
      .back-header
      {
        background-color:#AD0056;
      }
      .black-head
      {
          background: rgba(0,0,0,0.5);
          padding-bottom:20px;
          position:relative;
          top:-55px;
      }
    </style>

    @endsection
    @include('partials/donde',array())
    @section('content')
    <div class="container">
    <div class="row black-head">
          <div class="col-md-12 col-xs-12 title_ins2">
              <h1 >DELEGACIONES</h1>
          </div>
    </div>
    </div>

   <div class="container back">
      <div class="col-md-12">
          ¿DÓNDE ESTAMOS?
      </div>
      <div class="col-lg-12">
      <label>Selecciona la distancia, DISTANCIA ACTUAL: <span id="distancia">500</span> metros de distancia</label>
      <input type=range min=1 max=20 value=1 step=1 id="range">
      </div>
      <div class="col-md-12">
      <div id="map"></div>
      </div>
   </div>


    @endsection
    @section('modals')

    @endsection
    @section('js-extras')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
     <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script type="text/javascript">
    var markers= []; var poly, geodesicPoly;
    var map;


    function initMap() {

        var center = {
        url: 'http://172.25.11.64/adb/public/assets/image/kiosco.png',
        // This marker is 20 pixels wide by 32 pixels high.
        scaledSize: new google.maps.Size(40, 40),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(0, 40)
      };
       var item = {
        url: 'http://172.25.11.64/adb/public/assets/image/delegacion.png',
        // This marker is 20 pixels wide by 32 pixels high.
        scaledSize: new google.maps.Size(40, 40),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(0, 40)
      };



  //image = 'http://localhost:8000/assets/image/kiosko.png';


      map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 19.434994, lng: -99.135146},
        zoom: 12
      });
      markers[0] = new google.maps.Marker({
        map: map,
        icon: center,
        position: {lat: 19.434994, lng: -99.135146}
      });
      markers[1] = new google.maps.Marker({
        map: map,
          icon: item,
        position: {lat: 19.3799807, lng: -99.1850041}
      });
      markers[2] = new google.maps.Marker({
        map: map,
           icon: item,
        position: {lat: 19.4076292, lng: -99.170698}
      });
      markers[3] = new google.maps.Marker({
        map: map,
           icon: item,
        position: {lat: 19.4076774, lng: -99.1538053}
      });
      markers[4] = new google.maps.Marker({
        map: map,
           icon: item,
        position: {lat: 19.424572, lng: -99.1513046}
      });
      markers[5] = new google.maps.Marker({
        map: map,
           icon: item,
        position: {lat: 19.4333796, lng: -99.1593172}
      });
      markers[6] = new google.maps.Marker({
        map: map,
           icon: item,
        position: {lat: 19.488824, lng: -99.1327833}
      });


      google.maps.event.addListener(markers[0], 'position_changed', update);
      google.maps.event.addListener(markers[1], 'position_changed', update);
    /*
      poly = new google.maps.Polyline({
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        map: map,
      });

      geodesicPoly = new google.maps.Polyline({
        strokeColor: '#CC0099',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        geodesic: true,
        map: map
      });
    */
      update();
    }

    function update() {

      var path = [markers[0].getPosition(), markers[1].getPosition()];
        /*
      poly.setPath(path);
      geodesicPoly.setPath(path);
      */
      var distance = google.maps.geometry.spherical.computeDistanceBetween(path[0],path[1]);
      var heading = google.maps.geometry.spherical.computeHeading(path[0], path[1]);
      document.getElementById('heading').value = heading;
      document.getElementById('origin').value = path[0].toString();
      document.getElementById('destination').value = path[1].toString();
      document.getElementById('distance').value = distance;
    }
    $(document).on("change","#range",function()
    {
        var metros=$("#range").val()/2 * 1000;
        $("#distancia").text(metros);
        var path = [markers[0].getPosition(),markers[1].getPosition(),markers[2].getPosition(),markers[3].getPosition(),markers[4].getPosition(),markers[5].getPosition(),markers[6].getPosition()];
        for(var i=1; i < path.length; i++)
        {
            distance = google.maps.geometry.spherical.computeDistanceBetween(path[0],path[i]);

            if(distance > metros)
            {
                //console.log("i:"+i+" se va");
               markers[i].setVisible(false);
            }
            else{
                //console.log("i:"+i+" se queda "+distance);
                 markers[i].setVisible(true);
            }
        }
    });
        </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBn5blIIcG0LCtdS9uslDnpGHSamFPDeUI&libraries=geometry&callback=initMap"></script>
    @endsection