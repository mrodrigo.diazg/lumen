<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="200px" height="200px" viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve">
<g>
	<g>
		<g>
			<rect x="32.683" y="77.911" fill="#EDEDED" width="136.047" height="98.019"/>
			<rect x="32.683" y="161.029" fill="#828A95" width="136.047" height="14.9"/>
			<rect x="32.683" y="156.313" fill="#029605" width="136.047" height="1.665"/>
			<rect x="32.683" y="157.979" fill="#FFFFFF" width="136.047" height="1.666"/>
			<rect x="32.683" y="159.645" fill="#D61834" width="136.047" height="1.665"/>
		</g>
		<g id="XMLID_1002_">
			<g>
				<rect x="128.076" y="96.161" fill="#5EB9BE" width="23.526" height="23.526"/>
			</g>
			<g id="XMLID_1003_" opacity="0.43">
				<polygon fill="#000100" points="145.586,96.161 145.586,113.672 128.076,113.672 128.076,119.688 151.603,119.688
					151.603,96.161 				"/>
			</g>
			<g>
				<path fill="#FFFFFF" d="M151.943,120.029h-24.209V95.82h24.209V120.029z M128.416,119.348h22.846V96.501h-22.846V119.348z"/>
			</g>
		</g>
		<g id="XMLID_1004_">
			<g>
				<rect x="88.069" y="96.161" fill="#5EB9BE" width="23.526" height="23.526"/>
			</g>
			<g id="XMLID_1005_" opacity="0.43">
				<polygon fill="#000100" points="105.578,96.161 105.578,113.672 88.069,113.672 88.069,119.688 111.596,119.688 111.596,96.161
									"/>
			</g>
			<g>
				<path fill="#FFFFFF" d="M111.936,120.029H87.729V95.82h24.207V120.029z M88.41,119.348h22.845V96.501H88.41V119.348z"/>
			</g>
		</g>
		<g id="XMLID_1006_">
			<g>
				<rect x="48.062" y="96.161" fill="#5EB9BE" width="23.526" height="23.526"/>
			</g>
			<g id="XMLID_1007_" opacity="0.43">
				<polygon fill="#000100" points="65.57,96.161 65.57,113.672 48.062,113.672 48.062,119.688 71.588,119.688 71.588,96.161
					"/>
			</g>
			<g>
				<path fill="#FFFFFF" d="M71.929,120.029H47.721V95.82h24.208V120.029z M48.402,119.348h22.846V96.501H48.402V119.348z"/>
			</g>
		</g>
		<g>
			<rect x="32.683" y="5.717" fill="#EDEDED" width="136.047" height="72.194"/>
		</g>
		<g id="XMLID_114_">
			<g>
				<rect x="128.076" y="27.091" fill="#5EB9BE" width="23.526" height="23.527"/>
			</g>
			<g id="XMLID_115_" opacity="0.43">
				<polygon fill="#000100" points="145.586,27.091 145.586,44.602 128.076,44.602 128.076,50.619 151.603,50.619 151.603,27.091
									"/>
			</g>
			<g>
				<path fill="#FFFFFF" d="M151.943,50.959h-24.209V26.75h24.209V50.959z M128.416,50.278h22.846V27.432h-22.846V50.278z"/>
			</g>
		</g>
		<g id="XMLID_112_">
			<g>
				<rect x="88.069" y="27.091" fill="#5EB9BE" width="23.526" height="23.527"/>
			</g>
			<g id="XMLID_113_" opacity="0.43">
				<polygon fill="#000100" points="105.578,27.091 105.578,44.602 88.069,44.602 88.069,50.619 111.596,50.619 111.596,27.091
					"/>
			</g>
			<g>
				<path fill="#FFFFFF" d="M111.936,50.959H87.729V26.75h24.207V50.959z M88.41,50.278h22.845V27.432H88.41V50.278z"/>
			</g>
		</g>
		<g id="XMLID_1008_">
			<g>
				<rect x="128.076" y="137.502" fill="#5EB9BE" width="23.526" height="23.527"/>
			</g>
			<g id="XMLID_1009_" opacity="0.43">
				<polygon fill="#000100" points="145.586,137.502 145.586,155.012 128.076,155.012 128.076,161.029 151.603,161.029
					151.603,137.502 				"/>
			</g>
			<g>
				<path fill="#FFFFFF" d="M151.943,161.371h-24.209v-24.21h24.209V161.371z M128.416,160.688h22.846v-22.845h-22.846V160.688z"/>
			</g>
		</g>
		<g>
			<rect x="63.118" y="131.92" fill="#FFFFFF" width="33.803" height="43.938"/>
		</g>
		<g>
			<rect x="66.413" y="134.695" fill="#11629A" width="27.212" height="41.234"/>
		</g>
		<g>
			<rect x="41.858" y="28.991" fill="#FFFFFF" width="31.802" height="43.939"/>
		</g>
		<g>
			<rect x="44.153" y="31.766" fill="#3C3C3B" width="27.212" height="41.234"/>
		</g>
		<g>
			<polygon fill="#F39200" points="54.375,36.092 44.153,31.766 44.153,73 54.375,73 			"/>
		</g>
		<polyline fill="#BC700C" points="44.153,73 47.75,73 47.75,33.288 44.153,31.766 		"/>
		<g id="XMLID_1010_">
			<circle fill="#FFFFFF" cx="70.872" cy="157.493" r="2.182"/>
		</g>
		<rect x="25.5" y="73.5" fill="#706F6F" width="147" height="5"/>
		<g>
			<rect x="30.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="36.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="42.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="48.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="54.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="60.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="66.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="72.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="78.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="84.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="90.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="96.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="102.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="108.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="114.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="120.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="126.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="132.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="138.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="144.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="150.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="156.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="162.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="168.5" y="55.5" fill="#706F6F" width="2" height="18"/>
			<rect x="27.5" y="52.5" fill="#706F6F" width="145" height="3"/>
		</g>
	</g>
	<g>
		<rect x="25.706" y="175.41" fill="#828A95" width="150" height="17.9"/>
		<rect x="89.409" y="184.46" fill="#535454" width="86.297" height="8.851"/>
		<g>
			<path fill="#535454" d="M72.888,188.006c0-0.469-0.38-0.851-0.851-0.851c-0.47,0-0.851,0.382-0.851,0.851v5.39h1.702V188.006z"/>
			<path fill="#535454" d="M83.288,188.006c0-0.469-0.38-0.851-0.851-0.851c-0.47,0-0.851,0.382-0.851,0.851v5.39h1.701V188.006z"/>
			<path fill="#535454" d="M78.089,188.006c0-0.469-0.381-0.851-0.851-0.851c-0.47,0-0.851,0.382-0.851,0.851v5.39h1.702V188.006z"
				/>
			<path fill="#535454" d="M57.288,188.006c0-0.469-0.38-0.851-0.851-0.851c-0.47,0-0.851,0.382-0.851,0.851v5.39h1.701V188.006z"/>
			<path fill="#535454" d="M67.688,188.006c0-0.469-0.381-0.851-0.851-0.851c-0.47,0-0.851,0.382-0.851,0.851v5.39h1.702V188.006z"
				/>
			<path fill="#535454" d="M62.488,188.006c0-0.469-0.381-0.851-0.851-0.851c-0.47,0-0.852,0.382-0.852,0.851v5.39h1.702V188.006z"
				/>
		</g>
		<polygon fill="#535454" points="89.812,184.46 49.514,184.46 49.514,184.46 25.706,184.46 25.706,193.311 49.514,193.311
			49.514,184.887 89.812,184.887 		"/>
	</g>
</g>
</svg>
