<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>SEDESOL CONTIGO</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">


        <!--script async defer
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCpTvQacyDTlWWxJgj92xCR5NiktiG1oc&callback=initMap">
        </script-->
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<!--link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css"-->

<link href="http://192.268.41.1/public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="http://192.268.41.1/public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<!-- Global styles END -->

<!-- Page level plugin styles BEGIN -->
<link href="../../public/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<!-- Page level plugin styles END -->

    <!-- FONTS -->

<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700,700italic' rel='stylesheet' type='text/css'>


<!-- Theme styles BEGIN -->

<link href="http://192.268.41.1/public/assets/global/css/components.css" rel="stylesheet">
<link href="http://192.268.41.1/public/assets/frontend/onepage/css/style.css" rel="stylesheet">
<link href="http://192.268.41.1/public/assets/frontend/onepage/css/style-responsive.css" rel="stylesheet">
<link href="http://192.268.41.1/public/assets/frontend/onepage/css/themes/red.css" rel="stylesheet" id="style-color">
<link href="http://192.268.41.1/public/assets/frontend/onepage/css/custom.css" rel="stylesheet">
<link href="http://192.268.41.1/public/assets/css/style.css" rel="stylesheet">
@yield('style')

{{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

@yield('css-extras')
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>

<body class="page-header-fixed page-quick-sidebar-over-content">

    <script>
        function timer(){
            $(".modal-success").hide('fast');
            setInterval(redirect, 3000);
        }
        function redirect(){
            $("modal-success").css('display', 'none');
        }
        setInterval(timer, 2000);
    </script>

<!-- BEGIN HEADER -->
<!-- END HEADER -->
<div class="clearfix">
</div>

<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="container">
            @yield('content')
        </div>
    </div>
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<script src="http://192.268.41.1/public/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="http://192.268.41.1/public/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="http://192.268.41.1/public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Core plugins END (For ALL pages) -->

<!-- BEGIN RevolutionSlider -->

<!-- END RevolutionSlider -->

<!-- Core plugins BEGIN (required only for current page) -->
<script src="http://192.268.41.1/public/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="http://192.268.41.1/public/assets/global/plugins/jquery.easing.js"></script>
<script src="http://192.268.41.1/public/assets/global/plugins/jquery.parallax.js"></script>
<script src="http://192.268.41.1/public/assets/global/plugins/jquery.scrollTo.min.js"></script>
<script src="http://192.268.41.1/public/assets/frontend/onepage/scripts/jquery.nav.js"></script>
<!-- Core plugins END (required only for current page) -->

<!-- Global js BEGIN -->
<script src="http://192.268.41.1/public/assets/frontend/onepage/scripts/layout.js" type="text/javascript"></script>

<script>
    $(document).ready(function() {
      Layout.init();
    });
</script>

@yield('js-extras')
</body>
</html>